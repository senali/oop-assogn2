import java.io.IOException;
import java.util.List;

public class Main {
  public static void main(String[] args) throws IOException {
    String swimmer = "swimmer";
    String flyer = "flyer";
    String walker = "walker";

    Animal elephant = new Animal("Elephant", List.of(walker));
    Animal fish = new Animal("Fish", List.of(swimmer));
    Animal seaBird = new Animal("Sea bird", List.of(swimmer, flyer));
    Animal parrot = new Animal("Parrot", List.of(flyer));

    elephant.setFood("Coconut leaf");
    fish.setFood("Fish food");
    seaBird.setFood("fish");
    parrot.setFood("fruit");

    Zoo zoo = new Zoo();
    zoo.addAnimal(elephant);
    zoo.addAnimal(fish);
    zoo.addAnimal(seaBird);
    zoo.addAnimal(parrot);

    // send all animals to sleep
    zoo.sleepAllAnimals();
    System.out.println("");

    // wake all the animals
    zoo.wakeAllAnimals();
    System.out.println("");

    // all animals eat
    zoo.eatAllAnimals();
    System.out.println("");

    // get number all the animals:
    int noOfAllAnimals = zoo.noOfAllAnimals();

    // get all the swimmers and make them swim.
    zoo.swimmersSwim();
    System.out.println("");

    // get all the flyers and make them fly.
    zoo.flyersFly();
    System.out.println("");

    // get all the walkers and make them walk.
    zoo.walkersWalk();
    System.out.println("");

    // get the number of walker, flyers and swimmers separately.
    String noOfAnimalByCategory = zoo.noOfAnimalByCategory();

    // check if a animal is walker, flyer or swimmer
    zoo.isWalker(elephant);
    zoo.isFlyer(elephant);
    zoo.isSwimmer(elephant);

    System.out.println();
    System.out.println("No of all the animals in the zoo: " + noOfAllAnimals);

    System.out.println();
    System.out.println(noOfAnimalByCategory);
  }
}