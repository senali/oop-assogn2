import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Zoo {
  List<Animal> animals = new ArrayList<Animal>();

  /** send all animals to sleep */
  public void sleepAllAnimals() {
    for (Animal animal : animals) {
      animal.sleep();
    }
  }

  /** wake all the animals */
  public void wakeAllAnimals() {
    for (Animal animal : animals) {
      animal.wake();
    }
  }

  /** add a new animal to the zoo */
  public void addAnimal(Animal animal) {
    animals.add(animal);
  }

  /** all animals eat */
  public void eatAllAnimals() {
    for (Animal animal : animals) {
      animal.eat();
    }
  }

  /** get number all the animals */
  int noOfAllAnimals() {
    return animals.size();
  }

  /** get all the swimmers and make them swim */
  public void swimmersSwim() {
    for (Animal animal : animals) {
      boolean swimmer = animal.getCategories().contains("swimmer");
      if (swimmer) {
        System.out.println(animal.name + " is swimming.");
      }
    }
  }

  /** get all the flyers and make them fly */
  public void flyersFly() {
    for (Animal animal : animals) {
      boolean fly = animal.getCategories().contains("flyer");
      if (fly) {
        System.out.println(animal.name + " is fly.");
      }
    }
  }

  /** get all the walkers and make them walk */
  public void walkersWalk() {
    for (Animal animal : animals) {
      boolean walker = animal.getCategories().contains("walker");
      if (walker) {
        System.out.println(animal.name + " is walk.");
      }
    }
  }

  /** get the number of walker, flyers and swimmers separately */
  String noOfAnimalByCategory() {
    Map<String, Integer> categoryCountMap = new HashMap<>();
    for (Animal animal : animals) {
      for (String category : animal.getCategories()) {
        String categoryName = category;
        categoryCountMap.put(categoryName, categoryCountMap.getOrDefault(categoryName, 0) + 1);
      }
    }
    StringBuilder result = new StringBuilder();
    categoryCountMap.forEach((category, count) -> result.append(category).append(": ").append(count).append("\n"));
    return result.toString();
  }

  /** check if a animal is walker,flyer or swimmer **/
  public boolean checkAnimalType(Animal animal, String category) {
    List<String> categories = animal.getCategories();
    return categories.contains(category);
  }

  public void isWalker(Animal animal) {
    boolean walker = checkAnimalType(animal, "walker");
    if (walker) {
      System.out.println(animal.name + " is a Walker.");
    } else {
      System.out.println(animal.name + " is not a Walker.");
    }
  }

  public void isFlyer(Animal animal) {
    boolean flyer = checkAnimalType(animal, "flyer");
    if (flyer) {
      System.out.println(animal.name + " is a Flyer.");
    } else {
      System.out.println(animal.name + " is not a Flyer.");
    }
  }

  public void isSwimmer(Animal animal) {
    boolean swimmer = checkAnimalType(animal, "swimmer");
    if (swimmer) {
      System.out.println(animal.name + " is a Swimmer.");
    } else {
      System.out.println(animal.name + " is not a Swimmer.");
    }
  }

}