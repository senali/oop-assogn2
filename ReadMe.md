### OOP Assignment 2:

#### Run the project:
```javac Main.java```
```java Main```

#### Requrements:
1. Create a suitable class structure for this requirement.
2. Provide functionality to add a new animal to the zoo.
3. Each animal should have functionality to eat, there respective food, and zoo should have functionality to make all animals eat.
4. Add a functionality to zoo to get number all the animals.
5. Implement functionality to zoo to get all the swimmers and make them swim.
6. Implement functionality to zoo to get all the flyers and make them fly.
7. Implement functionality to zoo to get all the walkers and make them walk.
8. Add functionality to zoo to get the number of walker, flyers and swimmers separately.
9. Implement functionality to check if a animal is walker,flyer or swimmer for each animal (isSwimmer,isWalker,isFlyer).
