import java.util.List;

public class Animal {
  public String name;
  public String sound;
  public String food;
  public List<String> categories;

  public Animal(String name, List<String> categories) {
    this.name = name;
    this.sound = "";
    this.food = "";
    this.categories = categories;
  }

  public void setFood(String food) {
    this.food = food;
  }

  public List<String> getCategories() {
    return categories;
  }

  public void sleep() {
    System.out.println(name + "is sleeping.");
  }

  public void wake() {
    System.out.println(name + "is awake up.");
  }

  public void eat() {
    System.out.println(name + " eat " + food + ".");
  }
}